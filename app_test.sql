-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2018 at 03:54 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_price` decimal(10,0) NOT NULL,
  `item_stock` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `item_name`, `item_price`, `item_stock`) VALUES
(1, 'Pena', '1200', 20),
(2, 'Book', '5000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_category_id` int(11) DEFAULT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_description` text NOT NULL,
  `news_posting_date` datetime NOT NULL,
  `news_posting_by` int(11) NOT NULL,
  `news_status` enum('Publish','Draft') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_category_id`, `news_title`, `news_description`, `news_posting_date`, `news_posting_by`, `news_status`) VALUES
(1, 1, 'a', '<p>a</p>', '2018-05-27 14:50:34', 1, 'Publish'),
(2, 1, 'b', '<p>b</p>', '2018-05-27 21:01:47', 2, 'Publish'),
(3, 1, 'c', 'c', '2018-05-27 00:00:00', 1, 'Publish'),
(4, 1, 'Menyibak Kegelapan', '<div><img alt=\"\" src=\"https://lh3.googleusercontent.com/--is_uICrtQI/Twgu5NImtRI/AAAAAAAAAOE/19i4n1GXdck/s800/menyibak.jpg\"><br></div><div><br></div>                    Kehidupan memang bagaikan sebuah permainan,<br>Kita tak tau apa hasil yang akan kita dapatkan,<br>Mungkin kita kalah, mungkin kita menang.<br><br>Propabilitas menetukan,<br>Berapa persen peluang yang didapat,<br>Tergantung bagaimana kita memanfaatkannya.<br><br>Jika kita seorang petualang,<br>Kita pun tak tau jalan seperti apa yang akan kita lalui,<br>Naik, tutun, berkelok atupun lurus.<br><br>Matematika mengibaratkan,<br>Seperti garis horizontal, vertikal dan diagonal.<br><br>Fisika pun tak mau kalah,<br>Dengan teorinya tentang gelombang,<br>Menyebutkan penghantaran suara seperti naik dan turun.<br><br>Begitupun music,<br>Yang menyusun setiap nadanya dari yang rendah sampai yang tinggi.<br><br>Realita pun berkata,<br>Seperti jarum jam yang selalu berputar,<br>Kadang di atas, kadang dibawah ke kanan maupun ke kiri.<br><br>Bagaimana jika kita berkaca ?<br>Kita diberi beberapa pilihan,<br>Kaca cembung, yang kecil dibesarkan,<br>Kaca cekung, yang besar dikecilkan,<br>Kaca datar, yang tinggi direndahkan,<br><br>Tak akan pernah sama keadaan sebenarnya.<br><br>Lebih baik bercerminlah dengan hati nurani,<br>Karena yang kecil tidak dibesarkan, yang besar tidak dikecilkan,<br>Yang rendah tidak ditinggikan, yang tinggi tidak direndahkan.<br><br>Tak ada sesuatu yang akan sempurna.<br><br>Yogyakarta<br>Januari, 2009<div><br></div><div><img alt=\"\" src=\"http://lh3.ggpht.com/_vz86KuWMkLk/S4s4lnat3aI/AAAAAAAAADo/Cf_YFp32d08/s144/DnD.png\"><br><br>Read more: <a target=\"_blank\" rel=\"nofollow\" href=\"http://dnd-sq.blogspot.com/2010/04/menyibak-kegelapan.html#ixzz5GjJom6KH\">Menyibak Kegelapan ~ DnD ~ Hanya Ingin Berbagi</a>&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"http://dnd-sq.blogspot.com/2010/04/menyibak-kegelapan.html#ixzz5GjJom6KH\">http://dnd-sq.blogspot.com/2010/04/menyibak-kegelapan.html#ixzz5GjJom6KH</a>                  </div>', '2018-05-27 00:00:00', 2, 'Publish');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `news_category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`news_category_id`, `category_name`, `category_description`) VALUES
(1, 'Headline', 'Headline News'),
(2, 'Technology', '<p>Technology News</p>');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `state_city` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`, `state_city`) VALUES
(2, 'West Sumatera', 'Padang'),
(3, 'Special Region of Yogyakarta', 'Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `name`, `email`, `level`) VALUES
(1, 'verri', '36471498d56905c10b75456b80264d3b', 'Verri Andriawan', 'verri@tiduronline.com', 0),
(2, 'dnd', '36471498d56905c10b75456b80264d3b', 'DnD', 'dnd_07june07@live.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `category_id` (`news_category_id`),
  ADD KEY `user_id` (`news_posting_by`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`news_category_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `news_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `category_id` FOREIGN KEY (`news_category_id`) REFERENCES `news_category` (`news_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`news_posting_by`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
