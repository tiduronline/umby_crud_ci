<?php
class Home extends CI_Controller {

	// public function index(){
	// 	$tmp['title'] = 'Home';
	// 	// $tmp['contents'] = $this->load->view('admin/dashboard/home', null, true);
	// 	$tmp['contents'] = 'front/home/home';
	// 	$this->load->view('front/layout/template', $tmp);
	// }

	// =========================================================================== view
	public function index(){
		// data konten
		$data['judul'] = "Home";
		$data['sub_judul'] = "Daftar Artikel";
		// $data['hasil'] = $this->Blog_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('front/home/home',$data,TRUE);
		$this->load->view('front/layout/template',$tmp);
	}

}
