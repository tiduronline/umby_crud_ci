<?php
class Home extends CI_Controller {

	public function __construct(){
  	parent::__construct();
        // Your own constructor code
		if ( $this->session->userdata("username") == "") {
			$this->session->set_userdata('message', "access denied");
			redirect("admin/login",'refresh');
		}
    }

	public function index(){

		// data content
		$data['judul'] = "Dashboard";
		$data['sub_judul'] = "Admin Dashboard";
		// $data['hasil'] = $this->News_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('admin/home/index',$data,TRUE);
		$this->load->view('admin/layout/template',$tmp);
	}
}
