<?php
class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index(){
		$tmp['title'] = 'Login';
		$this->load->view('admin/login', $tmp);
	}

	public function auth() {
		if($this->input->post('ip-username')=='' && $this->input->post('ip-pass')==''){
			$this->session->set_userdata('message', "inputan tidak boleh kosong");
			redirect('admin/login', 'refresh');
		}
		else {
			//rules
			$this->form_validation->set_rules('ip-username','username','trim|required|xss_clean');
			$this->form_validation->set_rules('ip-pass','password','trim|required|xss_clean');
			//disini terdapat callback : callback_check database
			//digunakan untuk memanggil function check_database() di bawah

			//jika validasi gagal maka akan langsung dikembalikan ke login
			if($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('message', "username / password anda tidk valid");
				redirect('admin/login', 'refresh');
			}
			else {
				$encrypt_pass = md5($this->input->post('ip-pass'));

				//mengecek kedua dengan cara mengecek database
				$username = $this->input->post('ip-username');
				$password = $encrypt_pass;

				$result = $this->Login_model->login($username, $password);

				//jika hasilnya ada maka masukan ke session field nama dan username dengan nama session login
				if($result){
					foreach ($result as $row) {
						$sess_array=array(
							'username'=>$row->username
						);
						$this->session->set_userdata('login', $sess_array);
						$this->session->set_userdata('username', $row->username);
					}
					$this->session->unset_userdata('message');
					redirect('admin/home', 'location');
				} else {
					$this->session->set_userdata('message', "username / password anda salah");
					redirect('admin/login', 'refresh');
				}
			}
		}
	}

	public function logout() {
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('onlocat');
		redirect('admin/login', 'refresh');
	}
}
