<?php 


class Mahasiswa extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Mahasiswa_model', 'Mahasiswa');
        $this->load->model('Province_model', 'Province');
        $this->load->model('Classes_model', 'Classes');
    }



    public function index() {
        $students = $this->Mahasiswa->get_all();
        $response = [
            'students'  => $students,
            'judul'     => 'Student List',
            'sub_judul' => ''
        ];

        $tmp['contents'] = $this->load->view('admin/mahasiswa/list', $response, True);
        $this->load->view('admin/layout/template', $tmp);
    }



    public function new() {
        $provinces = $this->Province->get_all();
        $provinces = $this->Province->form_to_dropdown($provinces);

        $classes = $this->Classes->get_all();
        $classes = $this->Classes->form_to_dropdown($classes);
        
        $response = [
            'provinces' => $provinces,
            'classes'   => $classes,
            'judul'     => 'Edit Student',
            'sub_judul' => ''
        ];

        $tmp['contents'] = $this->load->view('admin/mahasiswa/add', $response, True);
        $this->load->view('admin/layout/template', $tmp);
    }



    public function edit() {
        $nim = $this->input->get('nim');
        
        if(empty($nim) || !$nim) {
            $this->session->set_flashdata('message', "The student isn't in our database");
            redirect('admin/mahasiswa');
        } else {

            $student    = $this->Mahasiswa->get_by_nim($nim);

            $provinces  = $this->Province->get_all();
            $provinces  = $this->Province->form_to_dropdown($provinces);

            $classes    = $this->Classes->get_all();
            $classes    = $this->Classes->form_to_dropdown($classes);
           
            $response = [
                'student'   => $student,
                'provinces' => $provinces,
                'classes'   => $classes
            ];

            $tmp['contents'] = $this->load->view('admin/mahasiswa/edit', $response, True);
            $this->load->view('admin/layout/template', $tmp); 
        }
        
    }


    public function update() {
        
        $nim = $this->input->post('nim');
        if($nim) {
        
            $this->form_validation->set_rules($this->Mahasiswa->validators);

            if($this->form_validation->run() !== FALSE) {
                if($this->Mahasiswa->update($_POST)) {
                    $this->session->set_flashdata('message', "Student's data has been updated");
                } else {
                    $this->session->set_flashdata('message', "Student's data failed to be updated");
                }
            } 
        } else {
            $this->session->set_flashdata('message', "Student's data has been updated");
        } 

        $this->edit();
    }


    public function save() {

        $this->form_validation->set_rules($this->Mahasiswa->validators);

        if($this->form_validation->run() !== FALSE) {
            if($this->Mahasiswa->save($_POST)) {
                $this->session->set_flashdata('message', 'Student data has been saved');
            } else {
                $this->session->set_flashdata('message', 'Failed to save new data');
            }

            redirect('admin/mahasiswa');
        } else {
            $this->new();
        }

    }



    public function delete() {
        $nim = $this->input->get('nim');

        $is_deleted = $this->Mahasiswa->delete($nim);
        if($is_deleted) {
            $this->session->set_flashdata('message', "Student's data sucessfully deleted");
        } else {
            $this->session->set_flashdata('message', "Failed to delete student;s data");
        }

        redirect('admin/mahasiswa', 'refresh');
    }

}