<?php
class News_Category extends CI_Controller{

	public function __construct(){
    parent::__construct();
    // Your own constructor code
		if ( $this->session->userdata("username") == ""){
			redirect("admin/login",'refresh');
			$this->session->set_userdata('message', error("access denied"));
		}
  }

  // =========================================================================== view
	public function index(){
		// data konten
		$data['judul'] = "Kategori Berita";
		$data['sub_judul'] = "Daftar Kategori Berita";
		$data['hasil'] = $this->News_Category_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('admin/news_category/view',$data,TRUE);
		$this->load->view('admin/layout/template',$tmp);
	}

  // =========================================================================== Add
	public function add(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->form_validation->set_rules('news_title', 'Judul', 'trim|required|xss_clean');
			$this->form_validation->set_rules('news_description', 'Isi', 'trim|required|xss_clean');

			if ($this->form_validation->run() == FALSE){
				$data ['err'] = error_admin(validation_errors());
				$tmp['contents'] = $this->load->view("admin/news_category/add",$data,TRUE);
			}
			else{
				$aksi = $this->News_Category_model->add();
				if ($aksi){
					$this->session->set_flashdata("message",valid_admin("data berhasil disimpan"));
					redirect('admin/news_category','refresh');
				}
				else{
					$this->session->set_flashdata("message",error_admin("gagal menyimpan data baru"));
					redirect('admin/news_category/add','refresh');
				}
			}
		}

		$data['judul'] = "Kategori Berita";
		$data['sub_judul'] = "Tambah Data Kategori Berita";

		// load template
		$tmp['contents'] = $this->load->view("admin/news_category/add",$data,TRUE);
		$this->load->view("admin/layout/template",$tmp);
	}

  // =========================================================================== Edit
	public function edit($id=0){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			// get data news
			$data_news = $this->News_Category_model->get_detail_by_id($id);
			if (count($data_news) > 0){
				$aksi = $this->News_Category_model->update($id);
				if ($aksi){
					$this->session->set_flashdata("message",valid_admin("data berhasil disimpan"));
					redirect('admin/news_category','refresh');
				}
				else{
					$this->session->set_flashdata("message",error_admin("gagal mengubah data"));
					redirect('admin/news_category/edit/'.$id,'refresh');
				}
			}
			else{
				// jika tidak ditemukan data
				$this->session->set_flashdata("message",error_admin("data gagal diedit karena tidak ada data"));
				redirect('admin/news_category/edit/'.$id,'refresh');
			}
		}
		else{
			// get data news
			$data_news = $this->News_Category_model->get_detail_by_id($id);

			$data['judul'] = "Kategori Berita";
			$data['sub_judul'] = "Form Edit";
			$data['old_value'] = $this->News_Category_model->get_detail_by_id($id);

			// load template
			$tmp['contents'] = $this->load->view("admin/news_category/edit",$data,TRUE);
			$this->load->view("admin/layout/template",$tmp);
		}
	}

  // =========================================================================== Delete
	public function delete($id=0){
		// get data news
		$data_news = $this->News_Category_model->get_detail_by_id($id);
		if (count($data_news) > 0){
			// hapus data dari database
			$aksi = $this->News_Category_model->delete($id);

			if ($aksi){
				// jika query berhasil
				$this->session->set_flashdata("message",valid_admin("data berhasil dihapus"));
				redirect('admin/news_category','refresh');
			}
			else{
				// jika query gagal
				$this->session->set_flashdata("message",error_admin("data gagal dihapus karena gagal query"));
				redirect('admin/news_category','refresh');
			}
		}
		else{
			// jika tidak ditemukan data
			$this->session->set_flashdata("message",error_admin("data gagal dihapus karena tidak ada datanya"));
			redirect('admin/news_category','refresh');
		}
	}

}
