<?php
class State extends CI_Controller{

	public function __construct(){
    parent::__construct();
    // Your own constructor code
		if ( $this->session->userdata("username") == ""){
			redirect("admin/login",'refresh');
			$this->session->set_userdata('message', error("access denied"));
		}
  }

  // =========================================================================== view
	public function index(){
		// data konten
		$data['judul'] = "Provinsi";
		$data['sub_judul'] = "Daftar Provinsi";
		$data['hasil'] = $this->State_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('admin/state/view',$data,TRUE);
		$this->load->view('admin/layout/template',$tmp);
	}

  // =========================================================================== Add
	public function add(){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			$this->form_validation->set_rules('state_name', 'Nama Provinsi', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state_city', 'Ibu Kota', 'trim|required|xss_clean');

			if ($this->form_validation->run() == FALSE){
				$data ['err'] = error_admin(validation_errors());
				$tmp['contents'] = $this->load->view("admin/state/add",$data,TRUE);
			}
			else{
				$aksi = $this->State_model->add();
				if ($aksi){
					$this->session->set_flashdata("message",valid_admin("data berhasil disimpan"));
					redirect('admin/state','refresh');
				}
				else{
					$this->session->set_flashdata("message",error_admin("gagal menyimpan data baru"));
					redirect('admin/state/add','refresh');
				}
			}
		}

		$data['judul'] = "Provinsi";
		$data['sub_judul'] = "Tambah Data Provinsi";

		// load template
		$tmp['contents'] = $this->load->view("admin/state/add",$data,TRUE);
		$this->load->view("admin/layout/template",$tmp);
	}

  // =========================================================================== Edit
	public function edit($id=0){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			// get data news
			$data_state = $this->State_model->get_detail_by_id($id);
			if (count($data_state) > 0){
				$aksi = $this->State_model->update($id);
				if ($aksi){
					$this->session->set_flashdata("message",valid_admin("data berhasil disimpan"));
					redirect('admin/state','refresh');
				}
				else{
					$this->session->set_flashdata("message",error_admin("gagal mengubah data"));
					redirect('admin/state/edit/'.$id,'refresh');
				}
			}
			else{
				// jika tidak ditemukan data
				$this->session->set_flashdata("message",error_admin("data gagal diedit karena tidak ada data"));
				redirect('admin/state/edit/'.$id,'refresh');
			}
		}
		else{
			// get data news
			$data_state = $this->State_model->get_detail_by_id($id);

			$data['judul'] = "Provinsi";
			$data['sub_judul'] = "Form Edit";
			$data['old_value'] = $this->State_model->get_detail_by_id($id);

			// load template
			$tmp['contents'] = $this->load->view("admin/state/edit",$data,TRUE);
			$this->load->view("admin/layout/template",$tmp);
		}
	}

  // =========================================================================== Delete
	public function delete($id=0){
		// get data news
		$data_state = $this->State_model->get_detail_by_id($id);
		if (count($data_state) > 0){
			// hapus data dari database
			$aksi = $this->State_model->delete($id);

			if ($aksi){
				// jika query berhasil
				$this->session->set_flashdata("message",valid_admin("data berhasil dihapus"));
				redirect('admin/state','refresh');
			}
			else{
				// jika query gagal
				$this->session->set_flashdata("message",error_admin("data gagal dihapus karena gagal query"));
				redirect('admin/state','refresh');
			}
		}
		else{
			// jika tidak ditemukan data
			$this->session->set_flashdata("message",error_admin("data gagal dihapus karena tidak ada datanya"));
			redirect('admin/state','refresh');
		}
	}

}
