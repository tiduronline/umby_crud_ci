<?php
class User extends CI_Controller {

  public function __construct(){
    parent::__construct();
    // Your own constructor code
		if ( $this->session->userdata("username") == ""){
			redirect("admin/login",'refresh');
			$this->session->set_userdata('message', error("access denied"));
		}
  }

  // =========================================================================== view
	public function index(){
		// data konten
		$data['judul'] = "User";
		$data['sub_judul'] = "Daftar User";
		// $data['hasil'] = $this->User_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('admin/user/view',$data,TRUE);
		$this->load->view('admin/layout/template',$tmp);
	}

}
