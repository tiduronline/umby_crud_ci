<?php
class Blog extends CI_Controller {

	// =========================================================================== view
	public function index(){
		// data konten
		$data['judul'] = "Blog";
		$data['sub_judul'] = "Daftar Artikel";
		$data['hasil'] = $this->Blog_model->get_all();

		// load template and variabels
		$tmp['contents'] = $this->load->view('front/blog/blog_view',$data,TRUE);
		$this->load->view('front/layout/template',$tmp);
	}

	public function detail($id=0){
		// memanggil database
		$data['detail'] = $this->Blog_model->get_detail_by_id($id);

		$data['judul'] = "Blog";
		$tmp['contents'] = $this->load->view('front/blog/blog_detail',$data,TRUE);
		$this->load->view('front/layout/template',$tmp);
	}

}
