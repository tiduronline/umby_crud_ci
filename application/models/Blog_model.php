<?php
class Blog_model extends CI_Model{

  function __construct(){
      parent::__construct();
  }

  // ===========================================================================
	// Halaman front end
	// ===========================================================================

  // =========================================================================== View All
  function get_all(){
    $data = array();
		$this->db->select('*');
		$this->db->where('news.news_posting_by = user.id_user');
		$this->db->order_by('news.news_id desc');
		$this->db->limit(4);
		$Q = $this->db->get('news,user');

		if ($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row){
				$data[] = $row;
			}
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== View By ID
	function get_detail_by_id($id){
		$data = array();
		$this->db->select('*');
		$this->db->where('news.news_posting_by = user.id_user');
		$this->db->where('news.news_id',$id);
		$Q = $this->db->get('news,user');

		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}

		$Q->free_result();
		return $data;
	}
}
