<?php

class Classes_model extends CI_Model {

    public $table_name = 'classes';

    public function get_all() {
        $result = $this->db->get('classes');
        return $result->result_array();
    }


    public function form_to_dropdown($classes) {
        $options = [
            '0' => 'Select Class'
        ];
        foreach($classes as $class) {
            $class_name = $class['class_name'] .' '. $class['class_desc'];
            $options[$class['class_id']] = $class_name;
        }
        return $options;
    }

}