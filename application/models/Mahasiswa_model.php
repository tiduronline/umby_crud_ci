<?php

class Mahasiswa_model extends CI_Model {
    public $table_name = 'students';

    public $validators = [
        [   'field' => 'nim',
            'rules' => 'trim|required|exact_length[8]'],

        [   'field' => 'fullname',
            'rules' => 'trim|required' ],

        [   'field' => 'email',
            'rules' => 'trim|required|valid_email' ],

        [   'field' => 'phone',
            'rules' => 'trim|required' ],

        [   'field' => 'gender',
            'rules' => 'required' ],
        [
            'field' => 'birth_place',
            'rules' => 'trim|required' ],

        [   'field' => 'date',
            'rules' => 'required|greater_than[0]' ],

        [   'field' => 'month',
            'rules' => 'required|greater_than[0]' ],
        
        [   'field' => 'year',
            'rules' => 'required|greater_than[0]' ],
        
        [   'field' => 'address_loc',
            'rules' => 'trim|xss_clean'],

        [   'field' => 'province_id',
            'rules' => 'required|greater_than[0]' ],

        [   'field' => 'class_id',
            'rules' => 'required|greater_than[0]']
    ];


    public function __construct() {
        parent::__construct();

        $CI =& get_instance();
        $CI->load->model('Profile_model', 'Profile');
        $CI->load->model('Classes_model', 'Classes');
    }


    public function get_all() {
        $this->db->select('*');

        $profile_tbl    = $this->Profile->table_name;
        $class_tbl      = $this->Classes->table_name;

        $this->db->join($profile_tbl, 
                        $this->table_name.'.nim = '.$profile_tbl.'.nim',
                        'left');

        $this->db->join($class_tbl, 
                        $this->table_name.'.class_id = '.$class_tbl.'.class_id', 
                        'left');

        $result = $this->db->get($this->table_name);

        return $result->result_array();
    }


    public function get_by_nim() {
        $nim            = $this->input->get('nim');
        $profile_tbl    = $this->Profile->table_name;
        $class_tbl      = $this->Classes->table_name;

        $this->db->join($profile_tbl, 
                        $this->table_name.'.nim = '.$profile_tbl.'.nim', 
                        'left');

        $this->db->join($class_tbl,
                        $class_tbl.'.class_id = '.$class_tbl.'.class_id', 
                        'left');

        $result = $this->db->get_where($this->table_name, [$this->table_name.'.nim' => $nim]);
        return $result->row_array();
    }


    public function update($data) {
        $result = $this->db->get_where($this->table_name, ['nim' => $data['nim']]);
        if($result->row()) {

            $this->db->trans_start();
            $student_params = [
                "fullname"  => $data['fullname'],
                "email"     => $data['email'],
                "phone"     => $data['phone'],
                "gender"    => $data['gender'],
                "class_id"  => $data['class_id'],
                "active"    => isset($data['active'])  ? 1 : 0
            ];

            
            $result = $this->db->update($this->table_name, $student_params, ["nim" => $data['nim']]);

            if($result) {
                $birth_date = $data['year'].'-'.$data['month'].'-'.$data['date'];

                $profile_params = [
                    "birth_place"   => $data['birth_place'],
                    "birth_date"    => $birth_date,
                    "province_id"   => $data['province_id'],
                    "address_loc"   => $data['address_loc'],
                ];

                $is_success = $this->db->update($this->Profile->table_name, $profile_params, ["nim" => $data['nim']]);
                $this->db->trans_complete();

                return $is_success;
            }
        }
    }


    public function save($data) {
        $result = $this->db->get_where($this->table_name, ['nim' => $data['nim']]);
        
        if(!$result->row()) {
            $birth_date = $data['year'].'-'.$data['month'].'-'.$data['date'];

            $params = [
                "nim"       => $data['nim'],
                "fullname"  => $data['fullname'],
                "email"     => $data['email'],
                "phone"     => $data['phone'],
                "gender"    => $data['gender'],
                "class_id"  => $data['class_id'],
                "active"    => isset($data['active']) ?  1 :0
            ];


            // Open transaction Database
            $this->db->trans_start();
            $result = $this->db->insert($this->table_name, $params);


            if($result) {
                $params = [
                    "nim"           => $data['nim'],
                    "birth_place"   => $data['birth_place'],
                    "birth_date"    => $birth_date,
                    "province_id"   => $data['province_id'],
                    "address_loc"   => $data['address_loc']
                ];
                
                $is_success = $this->db->insert($this->Profile->table_name, $params);

                // Commit all the things
                $this->db->trans_complete();
                return $is_success;
            }
        } else {
            return false;
        }
    }



    public function delete($nim) {
        $result = $this->db->get_where($this->table_name, ['students.nim' => $nim]);
        if($result->row()) {

            // if your reference column not using cascade
            // --------------------------------------------------------------------------------
            // $sql = $this->db->delete($this->Profile->table_name, ["nim" => $nim]);
            // if($sql) {
            //     $sql = $this->db->delete($this->table_name, ["nim" => $nim]);
            // }
            
            $result = $this->db->delete($this->table_name, ["nim" => $nim]);
            if($result) { return True; }
        }
        return false;
    }


}
