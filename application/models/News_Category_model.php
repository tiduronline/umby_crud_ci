<?php
class News_Category_model extends CI_Model{

  function __construct(){
      parent::__construct();
  }

  // ===========================================================================
	// fungsi-fungsi yang digunakan di halaman back-end (oleh admin)
	// ===========================================================================

  // =========================================================================== View All
	function get_all(){
		$data = array();
		$this->db->select('*');
		$this->db->order_by('news_category.news_category_id DESC');
		$Q = $this->db->get('news_category');

		if ($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row){
				$data[] = $row;
			}
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== View By ID
	function get_detail_by_id($id){
		$data = array();
		$this->db->select('*');
		$this->db->where('news_category.news_category_id',$id);
		$Q = $this->db->get('news_category');

		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== Add
	function add(){

		$data = array(
						'category_name' => $this->input->post('category_name'),
						'category_description' => $this->input->post('category_description')
					);

		$action = $this->db->insert('news_category', $data);

		return $action;
	}

  // =========================================================================== Edit
	function update($id){
		$data = array(
						'category_name' => $this->input->post('category_name'),
						'category_description' => $this->input->post('category_description')
					);

		$this->db->where('news_category_id',$id);
		$action = $this->db->update('news_category', $data);

		return $action;
	}

  // =========================================================================== Delete
	function delete($id){
		$this->db->where('news_category_id', $id);
		$action = $this->db->delete('news_category');
		return $action;
	}

}
