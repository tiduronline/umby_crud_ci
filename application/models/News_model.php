<?php
class News_model extends CI_Model{

  function __construct(){
      parent::__construct();
  }

  // ===========================================================================
	// halaman back-end
	// ===========================================================================

  // =========================================================================== View All
	function get_all(){
		$data = array();
		$this->db->select('*');
		$this->db->where('news.news_posting_by = user.id_user');
		$this->db->order_by('news.news_id desc');
		$Q = $this->db->get('news,user');

		if ($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row){
				$data[] = $row;
			}
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== View By ID
	function get_detail_by_id($id){
		$data = array();
		$this->db->select('*');
		$this->db->where('news.news_posting_by = user.id_user');
		$this->db->where('news.news_id',$id);
		$Q = $this->db->get('news,user');

		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== Add
	function add(){
		date_default_timezone_set('Asia/Jakarta');
		$tgl_skr = date('Y-m-d H:i:s');
    $status = "Publish";
    $category ="1";

    // get id_user user
    $data = array();
		$this->db->select('*');
		$this->db->where('user.username',$this->session->userdata("username"));
		$Q = $this->db->get('user');
		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}
		$Q->free_result();
    $id_user = $data['id_user'];

		$data = array(
						'news_title' => $this->input->post('news_title'),
						'news_description' => $this->input->post('news_description'),
            'news_category_id' => "$category",
						'news_posting_date' => "$tgl_skr",
						'news_posting_by' => "$id_user",
            'news_status' => "$status"
					);

		$action = $this->db->insert('news', $data);
		return $action;
	}

  // =========================================================================== Edit
	function update($id){
		$data = array(
						'news_title' => $this->input->post('news_title'),
						'news_description' => $this->input->post('news_description')
					);

		$this->db->where('news_id',$id);
		$action = $this->db->update('news', $data);

		return $action;
	}

  // =========================================================================== Delete
	function delete($id){
		$this->db->where('news_id', $id);
		$action = $this->db->delete('news');
		return $action;
	}

}
	// ===========================================================================
	// halaman front-end -> pindah di Blog_model.php
	// ===========================================================================

  // =========================================================================== View Limit 4
// 	function get_top_4(){
// 		$data = array();
// 		$this->db->select('*');
// 		$this->db->where('news.news_posting_by = user.id_user');
// 		$this->db->order_by('news.news_id desc');
// 		$this->db->limit(4);
// 		$Q = $this->db->get('news,user');
//
// 		if ($Q->num_rows() > 0){
// 			foreach ($Q->result_array() as $row){
// 				$data[] = $row;
// 			}
// 		}
//
// 		$Q->free_result();
// 		return $data;
// 	}
//
//   // =========================================================================== Total Berita
// 	function get_ttl_row(){
// 		$qry = $this->db->query('SELECT * FROM news');
//
// 		$count = $qry->num_rows();
// 		//echo "$count";
// 		if ($count > 0){
// 			$total_row = $count;
// 		}
// 		else{
// 			$total_row = 0;
// 		}
// 		return $total_row;
// 	}
//
// 	function get_page($num, $offset){
// 		$data = array();
// 		$this->db->select('*');
// 		$this->db->where('news.news_posting_by = user.id_user');
// 		$this->db->order_by('news.news_id desc');
// 		$this->db->limit(3);
// 		$Q = $this->db->get('news,user',$num, $offset);
//
// 		if ($Q->num_rows() > 0){
// 			foreach ($Q->result_array() as $row){
// 				$data[] = $row;
// 			}
// 		}
//
// 		$Q->free_result();
// 		return $data;
// 	}
//
// }
