<?php

class Province_model extends CI_Model {

    public function get_all() {
        $result = $this->db->get('provinces');
        return $result->result_array();
    }


    public function form_to_dropdown($provinces) {
        $options = [
            '0' => 'Select Province' 
        ];
        foreach($provinces as $province) {
            $options[$province['province_id']] = $province['province_name'];
        }

        return $options;
    }
    
}