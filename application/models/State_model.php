<?php
class State_model extends CI_Model{

  function __construct(){
      parent::__construct();
  }

  // ===========================================================================
	// fungsi-fungsi yang digunakan di halaman back-end (oleh admin)
	// ===========================================================================

  // =========================================================================== View All
	function get_all(){
		$data = array();
		$this->db->select('*'); // SELECT * FROM state ORDER BY state.state_id DESC
		$this->db->order_by('state.state_id DESC');
		$Q = $this->db->get('state');

		if ($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row){
				$data[] = $row;
			}
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== View By ID
	function get_detail_by_id($id){
		$data = array();
		$this->db->select('*'); // SELECT * FROM state WHERE state.state_id='$id'
		$this->db->where('state.state_id',$id);
		$Q = $this->db->get('state');

		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}

		$Q->free_result();
		return $data;
	}

  // =========================================================================== Add
	function add(){ // INPUT state (state_name,state_city ) VALUES ('state_name','state_city' )

		$data = [
						'state_name' => $this->input->post('state_name'),
						'state_city' => $this->input->post('state_city')
					];

		$action = $this->db->insert('state', $data);

		return $action;
	}

  // =========================================================================== Edit
	function update($id){ // UPDATE state SET state_name='state_name',
                                  //  state_city='state_city'
                                  // WHERE state_id = $id
		$data = [
						'state_name' => $this->input->post('state_name'),
						'state_city' => $this->input->post('state_city')
					];

		$this->db->where('state_id',$id);
		$action = $this->db->update('state', $data);

		return $action;
	}

  // =========================================================================== Delete
	function delete($id){
		$this->db->where('state_id', $id); // DELETE FROM state WHERE state_id='$id'
		$action = $this->db->delete('state');
		return $action;
	}

}
