<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Students
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Students</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Main row -->
    <div class="row">
        
        <div class="col-md-12">

        <?php 
        $errors = validation_errors(); 
        if(!empty($errors)): ?>
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <?php echo $errors ?>
                </div>
            </div>
        </div> 
        <?php endif ?>

        
        <?php echo form_open_multipart('/admin/mahasiswa/save', 
                    ['role' => 'form']); 
        ?>


        <div class="col-md-12">
        
            <div class="box box-primary">
                <div class="box-header with-border"></div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <div class="col-md-6">
                        
                        
                        <div class="form-group">
                            <?php echo form_label('NIM', 'nim'); ?>
                            <?php echo form_input('nim', set_value('nim', ''), 
                                                  [
                                                      'class' => 'form-control',
                                                      'placeholder' => 'NIM'
                                                  ]);
                            ?>
                        </div>


                        <div class="form-group">
                            <?php 
                                echo form_label('Name', 'fullname'); 
                                echo form_input('fullname', set_value('fullname', ''), 
                                                [  
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Nama'
                                                ]);
                            ?>
                        </div>


                        <div class="form-group">
                            <?php
                                echo form_label('Email', 'email'); 
                                echo form_input('email', set_value('email'), 
                                                [  
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Email',
                                                ]);
                            ?>
                        </div>


                        <div class="form-group">
                            <?php
                                echo form_label('Phone', 'phone'); 
                                echo form_input('phone', set_value('phone'), 
                                                [  
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Phone',
                                                ]);
                            ?>
                        </div>


                        <div class="form-group">
                            <?php 
                                echo form_label('Kelas', 'class_id');
                                echo form_dropdown( 'class_id', $classes, set_value('class_id'),
                                                    ['class' => "form-control"]);
                            ?>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('Gender', 'gender'); ?>
                            <div class="col-md-12">
                                
                                <div class="radio">
                                    <label>
                                    <?php 
                                    if(!isset($_POST['gender'])) {
                                        echo form_radio('gender', 'm', True);
                                    } else {
                                        echo form_radio('gender', 'm', (set_value('gender', '') === 'm'));
                                    } ?>
                                    Male
                                    </label>
                                </div>
                            
                                <div class="radio">
                                    <label>
                                    <?php echo form_radio('gender', 'f', (set_value('gender', '') === 'f')) ?>
                                    Female
                                    </label>
                                </div>
                                
                            </div>
                        </div>
                        
                        <?php if(isset($student)) {
                            $birth_date = explode('-', $student['birth_date']);
                        } ?>

                        <div class="form-group">
                            <label>Birth Date</label>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <select class="form-control" name="date">
                                        <option value="0" selected="selected">Date</option>
                                        <?php 
                                        $date = (int)set_value('date', 0);
                                        for($i = 1; $i<=31; $i++): ?>
                                            <option value="<?php echo $i ?>" 
                                                <?php echo ($date === $i) ? 'selected="selected"' : '' ?> ><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <?php 
                                        $months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug',
                                                    'Sep', 'Okt', 'Nov', 'Des'];
                                    ?>
                                    <select class="form-control" name="month">
                                        <option value="0" selected="selected">Month</option>
                                        <?php 
                                        $month = (int)set_value('month', 0);
                                        foreach($months as $idx => $value): ?>
                                            <option value="<?php echo ($idx + 1) ?>" 
                                                <?php echo ($month === ($idx+1)) ? 'selected="selected"' : '' ?>><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <?php
                                    $cur_year = date('Y');
                                    $start_year = $cur_year - 60;
                                    ?>
                                    <select class="form-control" name="year">
                                        <option value="0" selected="selected">Year</option>
                                        <?php while($cur_year > $start_year): ?>
                                            <option value="<?php echo $cur_year?>" 
                                                <?php 
                                                    $year = (int)set_value('year', 0);
                                                    echo ($year === $cur_year) ? 'selected="selected"' : '' ?>>
                                                    <?php echo $cur_year ?>
                                                </option>
                                        <?php
                                            $cur_year -= 1; 
                                            endwhile; ?>
                                    </select>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <?php 
                                echo form_label('Birth Place', 'birth_place'); 
                                echo form_input('birth_place', set_value('birth_place', ''),
                                                [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Birh Place'
                                                ]);
                            ?>
                        </div>
                        

                        <div class="form-group">
                            <?php 
                                echo form_label('Province', 'province_id');
                                echo form_dropdown( 'province_id', 
                                                    $provinces, 
                                                    set_value('province_id', 0),
                                                    ['class' => "form-control"]);
                            ?>
                        </div>

                        

                        <div class="form-group">
                            <label>Address</label>
                            <?php 
                                echo form_textarea('address_loc', 
                                                   set_value('address_loc', ''), 
                                                   ['class' => 'form-control']);
                            ?>
                        </div>


                        <div class="form-group">
                            <?php echo form_label('Student Status', 'active'); ?>
                            <div class="checkbox">
                                <label>
                                   <?php 
                                    
                                    $status = False;
                                    if(!isset($_POST['active'])) {
                                        $status = True;
                                    } else {
                                        $status = set_checkbox('active');
                                    }
                                    echo form_checkbox('active', '1', $status); 
                                    
                                    ?> Active
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?php echo site_url('admin/mahasiswa') ?>" class="btn btn-default btn-flat pull-left">Cancel</a>
                    <button class="btn btn-success btn-flat pull-right">Save Student</button>
                </div>
            </div>   
            <!-- /.box-body -->
        </div>
    

        <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /.row (main row) -->

</section>
<!-- /.content -->
