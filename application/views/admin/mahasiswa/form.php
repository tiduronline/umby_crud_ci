<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Students
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Students</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Main row -->
    <div class="row">
        
        <div class="col-md-12">

        <?php 
        $errors = validation_errors(); 
        if(!empty($errors)): ?>
        <div class="box">
            <div class="box-body">
                <div class="col-md-12">
                    <?php echo $errors ?>
                </div>
            </div>
        </div> 
        <?php endif ?>

        
        <?php echo form_open('/admin/mahasiswa/save', ['role' => 'form']); ?>


        <div class="col-md-12">
        
            <div class="box box-primary">
                <div class="box-header with-border"></div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <div class="col-md-6">
                        <?php 
                        $nim = (isset($student['nim'])) ? $student['nim'] : set_value('nim', '');

                        if(!empty($nim) && !empty(set_value('_is_edited', ''))) : ?>
                            <input type="hidden" name="nim" value="<?php echo $nim ?>">
                            <input type="hidden" name="_is_edited" value="1" />
                        <?php else: ?>
                            <div class="form-group">
                                <label>NIM</label>
                                <input type="text" 
                                        class="form-control" 
                                        name="nim" 
                                        placeholder="NIM">
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" 
                                    class="form-control" 
                                    name="fullname" 
                                    placeholder="Name" 
                                    value=<?php echo (isset($student['fullname'])) ? $student['fullname'] : set_value('fullname', '') ?>>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" 
                                    class="form-control" 
                                    name="email" 
                                    placeholder="Email"
                                    value=<?php echo (isset($student['email'])) ? $student['email'] : set_value('email', '') ?>>
                        </div>

                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" 
                                    class="form-control" 
                                    name="phone" 
                                    placeholder="0000000"
                                    value=<?php echo (isset($student['phone'])) ? $student['phone'] : set_value('phone', '') ?>>
                        </div>


                        <div class="form-group">
                            <label>Kelas</label>
                            <select name="class_id" class="form-control">
                                <option value="">Select Class</option>
                                <?php foreach($classes as $klass): ?>
                                <option value="<?php echo $klass['class_id']?>"
                                    <?php
                                    $class_id = (isset($student)) ? $student['class_id'] : set_value('class_id', '');
                                    if(!empty($class_id)
                                        && $class_id === $klass['class_id']) {
                                        echo 'selected="selected"';
                                    }
                                    ?>><?php echo $klass['class_name'].' ('.$klass['class_desc'].')' ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Gender</label>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" 
                                                    name="gender" 
                                                    value="m" 
                                                    <?php 
                                                        if ( isset($student['gender']) 
                                                            && $student['gender'] === 'm') {
                                                                echo 'checked="checked"';
                                                        } else {
                                                            echo 'checked="checked"';
                                                        }
                                                    ?>> Male
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" 
                                                    name="gender" 
                                                    value="f" 
                                                    <?php 
                                                    if ( isset($student['gender']) 
                                                        && $student['gender'] === 'f') {
                                                                echo 'checked="checked"';
                                                        } 
                                                    ?>> Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label>Birth Date</label>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <select class="form-control" name="date">
                                        <option value="0" selected="selected">Date</option>
                                        <?php 
                                        $date = (isset($birth_date[2])) ? (int)$birth_date[2] : (int)set_value('date', ''); 
                                        for($i = 1; $i<=31; $i++): ?>
                                            <option value="<?php echo $i ?>" 
                                                <?php echo ($date === $i) ? 'selected="selected"' : '' ?> ><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <?php 
                                        $months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug',
                                                    'Sep', 'Okt', 'Nov', 'Des'];
                                    ?>
                                    <select class="form-control" name="month">
                                        <option value="0" selected="selected">Month</option>

                                        <?php 
                                        $month = (isset($birth_date[1])) ? (int)$birth_date[1] : (int)set_value('month', ''); 
                                        foreach($months as $idx => $value): ?>
                                            <option value="<?php echo ($idx + 1) ?>" 
                                                <?php echo ($month === ($idx+1)) ? 'selected="selected"' : '' ?>><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <?php
                                    $cur_year = date('Y');
                                    $start_year = $cur_year - 60;
                                    ?>
                                    <select class="form-control" name="year">
                                        <option value="0" selected="selected">Year</option>
                                        <?php while($cur_year > $start_year): ?>
                                            <option value="<?php echo $cur_year?>" 
                                                <?php 
                                                    $year = (isset($birth_date[0])) ? (int)$birth_date[0] : (int)set_value('year', '');
                                                    echo ($year === $cur_year) ? 'selected="selected"' : '' ?>>
                                                    <?php echo $cur_year ?>
                                                </option>
                                        <?php
                                            $cur_year -= 1; 
                                            endwhile; ?>
                                    </select>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label>Birth Place</label>
                            <input type="text" 
                                    class="form-control" 
                                    name="birth_place" 
                                    placeholder="Birth Place"
                                    value=<?php echo (isset($student['birth_place'])) ? $student['birth_place'] : set_value('birth_place', '') ?>>
                        </div>
                        
                        <div class="form-group">
                            <label>Province</label>
                            
                            <select name="province_id" class="form-control">
                                <option value="0">Select Province</option>
                                <?php foreach($provinces as $province): ?>
                                <option value="<?php echo $province['province_id']?>"
                                    <?php
                                    if(isset($student)
                                        && $student['province_id'] === $province['province_id']) {
                                        echo 'selected="selected"';
                                    }
                                    ?>><?php echo $province['province_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address_loc" class="form-control" cols="30" rows="5"><?php echo isset($student) ? $student['address_loc'] : set_value('address_loc') ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Student Status</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" 
                                            name="active" 
                                            <?php
                                            if(isset($student)) {
                                                if($student['active']) {
                                                    echo 'checked="checked"';
                                                } 
                                            } else {
                                                echo 'checked="checked"';
                                            } ?>> Active
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?php echo site_url('admin/mahasiswa') ?>" class="btn btn-default btn-flat pull-left">Cancel</a>
                    <button class="btn btn-success btn-flat pull-right">Save Student</button>
                </div>
            </div>   
            <!-- /.box-body -->
        </div>
    

        <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /.row (main row) -->

</section>
<!-- /.content -->
