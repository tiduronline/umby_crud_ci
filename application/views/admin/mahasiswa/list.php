<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Students
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Students</li>
    </ol>
</section>



<!-- Main content -->
<section class="content">
    
    
    <!-- Main row -->
    <div class="row">

    <?php
    $message = $this->session->flashdata('message');
    if(!empty($message)) : ?>

    <div class="col-md-12">
        <div class="alert alert-warning">
        <?php echo $message ?>
        </div> 
    </div>
    
    <?php endif; ?>
    
    

    <div class="col-md-12">
        <div class="box">
            
            <div class="box-header with-border">
                <div class="col-md-5 pull-left">
                    <h3>List Students</h3>
                </div>
                <div class="col-md-2 pull-right">
                    <a href="<?php echo site_url('/admin/mahasiswa/new')?>" 
                        type="button" 
                        class="btn btn-block btn-success btn-flat" 
                        style="margin-top:10px;">Add New</a>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Student Name</th>
                    <th style="width: 5px;">Class</th>
                    <th>NIM</th>
                    <th style="width: 100px">Action</th>
                </tr>

            <?php if (isset($students)): ?>
                <?php foreach($students as $idx => $student): ?>
                

                <tr>
                    <td><?php echo $idx+1 ?></td>
                    <td><?php echo $student['fullname'] ?></td>
                    <td><?php echo $student['class_name']?></td>
                    <td><?php echo $student['nim'] ?></td>
                    <td>
                        <a href="<?php echo site_url('/admin/mahasiswa/edit') ?>?nim=<?php echo $student['nim']?>">
                            <i class="fa fa-fw fa-pencil-square-o"></i>
                        </a>
                        <a href="<?php echo site_url('/admin/mahasiswa/delete') ?>?nim=<?php echo $student['nim'] ?>"
                           onclick="return confirm('Are you sure?')">
                            <i class="fa fa-fw fa-trash-o"></i>
                        </a>
                    </td>
                </tr>

                <?php endforeach; ?>
            <?php endif; ?>
            </table>
            </div>
            <!-- /.box-body -->


            <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
            </div>


        </div>
    </div>
    </div>
    <!-- /.row (main row) -->

</section>
<!-- /.content -->
  