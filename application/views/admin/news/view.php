<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul;?>
    <small>Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="./dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul;?></li>
  </ol>
</section>

<?php echo $this->session->flashdata("message");?>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">

        <div class="box-header with-border">
        	<h3 class="box-title"><?php echo $sub_judul;?></h3>
          <?php echo anchor('admin/news/add','<button class="btn btn-primary btn-flat btn-sm pull-right">Add Berita</button>');?>
        </div><!-- /.box-header -->

        <div class="box-body">
<?php
if (count($hasil) > 0){
	$i=1;
?>
            <table id="tabeldata" class="table table-bordered table-striped" width="100%">
              <thead>
                <tr>
                  <th width="8%">No</th>
                  <th>Judul Berita</th>
                  <th width="13%">Tanggal</th>
                  <th width="11%">Upload by</th>
                  <th width="9%">Aksi</th>
                </tr>
              </thead>
              <tbody>
<?php
  foreach ($hasil as $key => $list){
    // Tampilkan hanya sebagian isi berita
    $news_content = strip_tags($list['news_description']);
    $content = substr($news_content,0,100); // ambil sebanyak 220 karakter
    $content = substr($news_content,0,strrpos($news_content," ")); // spasi antar kalimat
?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $list['news_title'];?></td>
                  <td><?php echo $list['news_posting_date'];?></td>
                  <td><?php echo $list['name'];?></td>
                  <td>
                    <a href='<?php echo "./news/edit/$list[news_id]";?>' title="Edit"><i class='fa fa-edit'></i></a>
                    &nbsp;&nbsp;&nbsp;
                    <a href='<?php echo "./news/delete/$list[news_id]";?>' title="Trash" onClick="return confirm('Delete Forever')">
                    <i class='fa fa-trash'></i></a>
                  </td>
                </tr>
<?php
$i++;
                  }
                  ?>
              </tbody>
            </table>
<?php
}
else{
?>
	<p class="text-muted">Data not available...</p>
<?php
}
?>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.cols -->
  </div><!-- /.row -->
</section><!-- /.content -->
