<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul;?>
    <small>Management </small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="./dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="./"><?php echo $judul;?></a></li>
    <li class="active">Add</li>
  </ol>
</section>

<?php echo $this->session->flashdata("message");?>
<?php if($_SERVER['REQUEST_METHOD'] == "POST") echo "$err"; ?>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
        	<h3 class="box-title"><?php echo $sub_judul;?></h3>
        </div><!-- /.box-header -->
<?php
  $attributes = array('autocomplete' => 'off','role' => 'form');
  echo form_open_multipart("admin/state/add",$attributes);
?>
        <div class="box-body">
        	<div class="form-group">
            <label for="state_name" class="control-label">Nama Provinsi</label>
            <input name="state_name" type="text" class="form-control" id="state_name" placeholder="Nama Provinsi" value="<?php echo set_value("state_name");?>" required>
          </div>
          <div class="form-group">
            <label for="category_description" class="control-label">Ibu Kota</label>
            <input name="state_city" type="text" class="form-control" id="state_city" placeholder="Ibu Kota" value="<?php echo set_value("state_city");?>" required>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary btn-sm btn-flat pull-right" style="margin-right: 5px;">Save</button>
<?php
  echo form_close();
?>
          <button class="btn btn-default btn-sm btn-flat pull-right" style="margin-right: 5px;" value="Cancel" onClick="self.history.back()">Cancel</button>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.cols -->
  </div><!-- /.row -->
</section><!-- /.content -->
