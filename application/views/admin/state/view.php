<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $judul;?>
    <small>Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="./dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo $judul;?></li>
  </ol>
</section>

<?php echo $this->session->flashdata("message");?>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">

        <div class="box-header with-border">
        	<h3 class="box-title"><?php echo $sub_judul;?></h3>
          <?php echo anchor('admin/state/add','<button class="btn btn-primary btn-flat btn-sm pull-right">Add Provinsi</button>');?>
        </div><!-- /.box-header -->

        <div class="box-body">
<?php
if (count($hasil) > 0){
	$i=1;
?>
            <table id="tabeldata" class="table table-bordered table-striped" width="100%">
              <thead>
                <tr>
                  <th width="8%">No</th>
                  <th width="23%">Nama Provinsi</th>
                  <th>Ibu Kota</th>
                  <th width="9%">Aksi</th>
                </tr>
              </thead>
              <tbody>
<?php
  foreach ($hasil as $key => $list){
?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $list['state_name'];?></td>
                  <td><?php echo $list['state_city'];?></td>
                  <td>
                    <a href='<?php echo "./state/edit/$list[state_id]";?>' title="Edit"><i class='fa fa-edit'></i></a>
                    &nbsp;&nbsp;&nbsp;
                    <a href='<?php echo "./state/delete/$list[state_id]";?>' title="Trash" onClick="return confirm('Delete Forever')"><i class='fa fa-trash'></i></a>
                  </td>
                </tr>
<?php
$i++;
                  }
                  ?>
              </tbody>
            </table>
<?php
}
else{
?>
	<p class="text-muted">Data not available...</p>
<?php
}
?>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.cols -->
  </div><!-- /.row -->
</section><!-- /.content -->
