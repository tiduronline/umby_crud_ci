<section id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <article>
<?php
if (count($detail) > 0){
?>
          <div class="post-quote">

            <div class="post-heading">
              <h3><a href="#"><?php echo $detail['news_title']; ?></a></h3>
            </div>
            <blockquote>
              <i class="icon-quote-left"></i> <?php echo $detail['news_description']; ?>
            </blockquote>
          </div>

          <div class="bottom-article">
            <ul class="meta-post">
              <li><i class="icon-calendar"></i><a href="#"> <?php echo $detail['news_posting_date']; ?></a></li>
              <li><i class="icon-user"></i><a href="#"> <?php echo $detail['name']; ?></a></li>
              <li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
              <li><i class="icon-comments"></i><a href="#">4 Comments</a></li>
            </ul>
            <a href='<?php echo "../";?>' title="Edit" class="pull-right">Back <i class="icon-angle-right"></i></a>
          </div>
<?php
}
else{
    echo "<p>Data not available...</p>";
}
?>
        </article>

      </div> <!--  col-lg-8 -->

      <div class="col-lg-4">
        <aside class="right-sidebar">
          <?php $this->load->view("front/layout/right_sidebar"); ?>
        </aside>
      </div> <!--  col-lg-4 -->
    </div>
  </div>
</section>
