<section id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <article>
<?php
if (count($hasil) > 0){
  foreach ($hasil as $key => $list){
  // Tampilkan hanya sebagian isi berita
    $news_content = strip_tags($list['news_description']);
    $content = substr($news_content,0,100); // ambil sebanyak 220 karakter
    $content = substr($news_content,0,strrpos($news_content," ")); // spasi antar kalimat
?>
          <div class="post-quote">

            <div class="post-heading">
              <h3><a href="#"><?php echo $list['news_title']; ?></a></h3>
            </div>
            <blockquote>
              <i class="icon-quote-left"></i> <?php echo $content; ?>
            </blockquote>
          </div>

          <div class="bottom-article">
            <ul class="meta-post">
              <li><i class="icon-calendar"></i><a href="#"> <?php echo $list['news_posting_date']; ?></a></li>
              <li><i class="icon-user"></i><a href="#"> <?php echo $list['name']; ?></a></li>
              <li><i class="icon-folder-open"></i><a href="#"> Blog</a></li>
              <li><i class="icon-comments"></i><a href="#">4 Comments</a></li>
            </ul>
            <a href='<?php echo "./blog/detail/$list[news_id]";?>' title="Edit" class="pull-right">
              Continue reading <i class="icon-angle-right"></i>
            </a>
          </div>
<?php
  }
}
else{
    echo "<p>Data not available...</p>";
}
?>
        </article>

        <div id="pagination">
          <span class="all">Page 1 of 3</span>
          <span class="current">1</span>
          <a href="#" class="inactive">2</a>
          <a href="#" class="inactive">3</a>
        </div> <!--  pagination -->
      </div> <!--  col-lg-8 -->

      <div class="col-lg-4">
        <aside class="right-sidebar">
          <?php $this->load->view("front/layout/right_sidebar"); ?>
        </aside>
      </div> <!--  col-lg-4 -->
    </div>
  </div>
</section>
