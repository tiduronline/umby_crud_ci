<meta charset="utf-8">
<title>Moderna - Bootstrap 3 flat corporate template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<!-- css -->
<link href="<?= base_url()?>asset/template/front/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?= base_url()?>asset/template/front/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="<?= base_url()?>asset/template/front/css/jcarousel.css" rel="stylesheet" />
<link href="<?= base_url()?>asset/template/front/css/flexslider.css" rel="stylesheet" />
<link href="<?= base_url()?>asset/template/front/css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link href="<?= base_url()?>asset/template/front/skins/default.css" rel="stylesheet" />

<!-- =======================================================
  Theme Name: Moderna
  Theme URL: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/
  Author: BootstrapMade
  Author URL: https://bootstrapmade.com
======================================================= -->
