<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("front/layout/header");?>
</head>

<body>
	<div id="wrapper">
		<!-- start header -->
		<header>
			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
          <?php $this->load->view("front/layout/menu");?>
				</div>
			</div>
		</header>
		<!-- end header -->

		<section id="featured">
			<!-- start slider -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!-- Slider -->
						<div id="main-slider" class="flexslider">
							<ul class="slides">
								<li>
									<img src="<?= base_url()?>asset/template/front/img/slides/1.jpg" alt="" />
									<div class="flex-caption">
										<h3>Modern Design</h3>
										<p>Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urna</p>
										<a href="#" class="btn btn-theme">Learn More</a>
									</div>
								</li>
								<li>
									<img src="<?= base_url()?>asset/template/front/img/slides/2.jpg" alt="" />
									<div class="flex-caption">
										<h3>Fully Responsive</h3>
										<p>Sodales neque vitae justo sollicitudin aliquet sit amet diam curabitur sed fermentum.</p>
										<a href="#" class="btn btn-theme">Learn More</a>
									</div>
								</li>
								<li>
									<img src="<?= base_url()?>asset/template/front/img/slides/3.jpg" alt="" />
									<div class="flex-caption">
										<h3>Clean & Fast</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit donec mer lacinia.</p>
										<a href="#" class="btn btn-theme">Learn More</a>
									</div>
								</li>
							</ul>
						</div>
						<!-- end slider -->
					</div>
				</div>
			</div>
		</section>

		<section id="content">
			<div class="container">
        <?php echo $contents; ?>
        <!-- <?php // $this->load->view($contents); ?> -->
			</div>
		</section>

    <footer>
      <?php $this->load->view("front/layout/footer");?>
		</footer>

	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?= base_url()?>asset/template/front/js/jquery.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/jquery.easing.1.3.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/bootstrap.min.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/jquery.fancybox.pack.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/jquery.fancybox-media.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/google-code-prettify/prettify.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/portfolio/jquery.quicksand.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/portfolio/setting.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/jquery.flexslider.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/animate.js"></script>
	<script src="<?= base_url()?>asset/template/front/js/custom.js"></script>

</body>

</html>
