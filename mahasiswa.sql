create table if not exists classes (
    class_id tinyint(3) unsigned not null auto_increment,
    class_name varchar(4) not null default "",
    class_desc varchar(255) null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp,
    primary key (class_id)
) engine=InnoDB;

create table if not exists students (
    nim char(8) not null default "",
    fullname varchar(150) not null default "",
    email varchar(30) not null default "",
    phone varchar(50) not null default "",
    gender enum('m', 'f') not null default "m",
    class_id tinyint(3) unsigned not null default 0,
    active boolean not null default true,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp,
    primary key (nim),
    foreign key (class_id) references classes(class_id) on delete no action on update no action,
    unique (email)
) engine=InnoDB;

create table if not exists provinces (
    province_id int(11) not null auto_increment,
    province_name varchar(100) not null default "",
    created_at timestamp not null default current_timestamp,
    updated_at timestamp not null default current_timestamp,
    primary key (province_id)
);

create table if not exists profiles (
    profile_id int(11) not null auto_increment,
    nim char(8) not null default "",
    birth_place varchar(100) null,
    birth_date date null,
    address_loc text null,
    province_id int(11) not null default 0,
    primary key (profile_id),
    foreign key (nim) references students(nim) on delete cascade,
    foreign key (province_id) references provinces(province_id) on delete no action
) engine=InnoDB;


insert into classes values  (1, '11', "R1 Kampus 1", current_timestamp, current_timestamp), 
                            (2, '21', 'R2 Kampus 1', current_timestamp, current_timestamp), 
                            (3, '31', 'R3 Kampus 1', current_timestamp, current_timestamp),
                            (4, '12', 'R1 Kampus 2', current_timestamp, current_timestamp),
                            (5, '22', 'R2 Kampus 2', current_timestamp, current_timestamp),
                            (6, '32', 'R3 Kampus 2', current_timestamp, current_timestamp),
                            (7, '13', 'R1 Kampus 3', current_timestamp, current_timestamp),
                            (8, '23', 'R2 Kampus 3', current_timestamp, current_timestamp),
                            (9, '33', 'R3 Kampus 3', current_timestamp, current_timestamp);


insert into students values ('14141414', 'Verri', 'verri@localhost.local', '000000000', 'm', 1, true, current_timestamp, current_timestamp),
                            ('15151515','Andri', 'andri@localhost.local', '000000000', 'm', 2, true, current_timestamp, current_timestamp),
                            ('16161616','Awan', 'awan@localhost.local', '000000000', 'm', 3, true, current_timestamp, current_timestamp);


insert into provinces values (1, 'DKI Jakarta', current_timestamp, current_timestamp),
                             (2, 'Jawa Barat', current_timestamp, current_timestamp),
                             (3, 'DI Yogyakarta',current_timestamp, current_timestamp);

insert into profiles values (1, '14141414', 'Jakarta', '1900-01-01', null, 1),
                            (2, '15151515', 'Bandung', '1900-01-01', null, 2), 
                            (3, '16161616', 'Yogyakarta', '1900-01-01', null, 3);
                            